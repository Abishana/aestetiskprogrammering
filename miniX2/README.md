# MiniX2
README  ·  _Abishana Tharumathas_

**EPILEPSI WARNING**
[Link til RUNME](https://abishana.gitlab.io/aestetiskprogrammering/miniX2/index.html)
_(Gør gerne browseren mindre, hvis canvasset er for stort (på mac trykker man på command og derefter -)_

[Link til js.fil](https://gitlab.com/Abishana/aestetiskprogrammering/-/blob/main/miniX2/miniX2sketch.js)

___

#### Hvad har jeg designet?

Mit design består af et canvas med to poop-emojis. Når cursoren er ved den venstre halvdel af canvasset, ses en regnbue-skiftende-farve poop-emoji oveni en brun baggrund. Denne poop emoji skal forestille sig at være langt fra perfekt idet afføring realistisk set overhovedet ikke er nydeligt. Dog er denne poop også urealistisk idet den skifter farve pr. frame.

Når cursoren rykkes over på højre side af canvasset ses en brun poop-emoji. Denne emoji ligner den poop-emoji som vi allerede har kendskab til. Den er glad, smilende og perfekt. Nu er det ikke emojien, der ændre farve pr. frame, men baggrunden. 
___

#### Hvordan har jeg programmeret mit design?
I miniX2 har jeg prøvet at udfordre mine evner ved at gøre designet interaktionsvenlig. Egentlig havde jeg intention om at designet skulle være i 3D. Dette havde jeg dog lidt svært ved, da jeg følte det gik for hurtigt op i niveau, hvis jeg skulle fokusere på både at programmere noget interaktionsvenligt, og i 3D. Derfor ændrede jeg mit fokus til kun at lave noget der var interaktionsvenligt. 

Mit canvas er sat til `createCanvas(1500,1500)`. Jeg har valgt at sætte bestemte værdier fremfor, at det skal fylde hele browseren. Dette har jeg valgt grundet interaktionen der sker, når cursoren flytter fra side til side på canvasset. Dette kommer jeg mere ind på senere i min ReadMe. 

Mine poop-emojis har jeg programmeret ved hjælp af flere geometriske koder:
`circle()`,`ellipse()`,`triangle()` og `arc()`

For både begge baggrunde og poops har jeg brugt forskellige farver. Jeg har en brun baggrund til den realistiske poop (regnbuefarvet poop) og i samme brune farve programmeret den perfekte poop. Til den realistiske poop og baggrunden bag den perfekte poop, har jeg programmeret til at skifte farve pr. frame. Derfor har jeg inddraget variabler ved at sætte dem ind i således:

let x = `random(0,255)`

Det samme har jeg gjort for y og z. Dette medvirker, at min poop og baggrund skifter farve, ved at vælge et random tal pr. frame for alle tre variabler x, y og z. Disse variabler har jeg indsat i koderne `background(x,y,z)` og `fill(x,y,z)`, for netop at få den regnbuefarvede poop og baggrund til at skifte farve pr. frame.

Til sidst har jeg lavet et `if statement`. Dette har jeg gjort, for netop at vise forskellen på begge emojis og baggrunde når cursoren er på hver sin side af canvasset. Derfor har jeg i mit `if statement` skrevet:

`if(mouseX/750)` 

Under dette `if statement` har jeg programmeret den ændrende baggrundsfarve og perfekte emoji (brune emoji), så det netop kun vises, når dette statement er true. Dette statement er true, når cursoren vertikalt er på den højre halvdel af canvasset, og dermed udføre det statement.
___


#### Hvorfor har jeg programmeret dette design?
Jeg har valgt at programmere dette design af flere årsager. For mig er poop emojien en særlig emoji. Jeg bruger den tit og i mange kontekster. Dog er konteksten sjælendt en direkte betydning til at jeg skidt. Det synes jeg er lidt sjovt, da den for mig mere har en betyding af at føle sig shitty eller ikke helt ved hvad man bør føle. 

**Hvorfor har jeg så lavet en mere realistisk emoji, som skifter farve?**
Dette har jeg valgt at gøre både af min personlige erfaring, men også for at skabe en form for sociokulturel debat. 
Poop-emojien vi kender til, findes kun i den ene brune farve. Hvis der er flere som ikke føler sig inklusive af de 5 hudfarver af emojis man kan vælge imellem, så tror jeg sagtens på, at man kan føle sig eksluderet, at denne poop-emoji ligner en perfekt chokolade softice. Men realistisk set, ser vores afføring ikke sådan ud, både i forhold til formen og farven...og afføring har ikke et ansigt.
Dog er det heller ikke realistisk at afføring er pink eller blåt. 

Hvorfor har jeg så inkluderet dette i mit design? 
Farver så vibrant som disse har jeg inkluderet, da mit budskab både omhandler realisme, men også inklusivitet. I et samfund som i dag, hvor man har plads til at skabe sin helt egen identitet og være en del af mange forskellige kommuniteter, var det relevant at inddrage denne farveskiftende faktor. Ved at inkudere denne farveskiftende faktor trækkes der både på debatter om, hvorvidt dette design/poop emojien er realistisk og inklusiv. Selv min version af poop emojien (regnbuefarvede emoji) er ikke 100% realistisk eller inklusiv. Dette er også intentionelt idet min version af emojien også i sig selv skaber debat indenfor bl.a. inklusivitet og realisme.

![screenshot](https://abishana.gitlab.io/aestetiskprogrammering/miniX2/Colourpoop.png)
![screenshot](https://abishana.gitlab.io/aestetiskprogrammering/miniX2/Ordinarypoop.png)
