
function setup() {
  // put setup code here
 
createCanvas(1500,1500);

}

function draw() {
  // put drawing code here  

  //Baggrund i farve af en poop
  background(60,40,20);

//Poop der skifter farve.
let x = random(0,255)
let y = random(0,255)
let z = random(0,255)

fill(x,y,z);
noStroke();
ellipse(500,750,600,150);
ellipse(470,690,540,150);
ellipse(490,630,500,150);
ellipse(530,570,420,150);
ellipse(400,500,350,150);
ellipse(570,460,400,150);
ellipse(480,400,300,150) ;

//Shade (brun plet på poop)
fill(60,40,20);
ellipse(670,680,60,20);

//1.øje 
fill(255);
circle(330,720,50);
fill(255,200,104);
ellipse(330,720,10,30) 
fill(0);
ellipse(330,720,3,20);

//2.øje
fill(255);
ellipse(590,380,100,50);
fill(230,240,120);
ellipse(610,380,40,35);
fill(0);
circle(620,380,20);

//Mund
fill(100,200,235);
arc(570,600,150,100,PI,0);
//Tunge
fill(230,50,160); 
ellipse(590,600,30,70);


/* Ændring af baggrundsfarve, når musen er ved
2. halvdel af skærmen. Her vises brun poop.*/
if(mouseX>750){

//Ændring af baggrundsfarve
background(x,y,z); 

//Brun poop
fill(60,40,20); 
noStroke();
ellipse(1000,1250,600,150);
ellipse(1000,1170,540,150);
ellipse(1000,1080,450,150);
ellipse(1000,990,300,150);
triangle(900,990,1000,840,1150,990);
  
//Øjne
fill(255);
ellipse(920,1070,80,100);
ellipse(1080,1070,80,100);
arc(1000,1170,150,100,0,PI);
  
fill(0);
circle(920,1090,60);
circle(1080,1090,60);
}
}
