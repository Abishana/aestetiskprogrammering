# MiniX4
README · _Abishana Tharumathas_

[Link til RUNME](https://abishana.gitlab.io/aestetiskprogrammering/miniX4/index.html)
(virker ikke helt så godt på safari)

[Link til js.fil](https://gitlab.com/Abishana/aestetiskprogrammering/-/blob/main/miniX4/miniX4sketch.js)


### ERROR
ERROR repræsenterer computerens "simple" måde at udtrykke en error på. Den visuelle simplicitet er resultatet af computerens arbejde bag skærmen. Designet er interaktivt idet baggrundsfarven, skriftfarven på "ERROR" og positionen af "ERROR" ændre sig, når man trykker musen ned på canvasset. Farverne udtrykker et hav af de binære koder og algoritmer der finder sted bag computeren, men det vi visuelt ser på skærmen er nærmest en overforsimplet billede af computerens fejl. Interaktionen er med til at vise computerens manglende evne til at gennemarbejde algoritmer, også idet man forsøger at interegere med computeren. 


##### Gennemgang af programmering
Min programmering er meget simpelt, hvilket også kan fornemmes på det visuelle design, da det også er simpelt. 
Jeg har startet med at angive variabler:

 `let t="ERROR"`  = Teksten der står på skærmen.

  `let x="150"`   = start x-position for teksten.

  `let y="150"`   = start y-position for teksten.

 
 Herefter har jeg under `function setup()` lavet canvasstørrelsen til browservinduets størrelse og angivet baggrundsfarven til grøn.
Under `function draw()` angives ordet "ERROR" dets font og størrelse. Da jeg intentionelt vil skriftfarven til at være sort, har jeg ikke selv angivet farven, da den automatisk vælger at farve teksten sort.

Den interaktive del kommer i spil ved `function mousePressed`. Når musen trykkes ned på canvasset, flytter ordet "ERROR" sig til musens position på canvasset. Dette har jeg gjort ved at tildele x og y en ny værdi, altså `x=mouseX` og `y=mouseY`.
Desuden skifter baggrundsfarven og skriftfarven sig for hver gang man trykker på musen. Baggrundsfarven ændre sig inden for grayscale værdierne, og skriftfarven ændre sig i forskellige nuancer af grøn.


###### Hvorfor har jeg valgt disse specifikke farver, når farveændringen finder sted under interaktionen?
Formålet med disse farver er at visualisere computerens arbejde bag skærmen samtidig med at vise det visuelle resultat vi som brugere får af dets arbejde.
"ERROR" er ordet der repræsenterer fejlet bag computeren, altså fejlet i dets arbejde. Farverne på grayscalen og de grønne nuancer har jeg valgt idet man flere steder ser binære koder stå med grøn tekst. 


#### "Capture All"
Temaet "Capture All" kommer til udtryk idet designet består af en visuel tomhed. En simpel baggrund og ERROR der skifter farve og skifter position for hvor musen befinder sig. Dog bag denne ERROR lægger der en hel process af algoritmer og arbejde. Computerens indviklede process er visuelt forsimplet for brugeren.

Jeg havde under designprocessen overvejet at få ERROR til at ændre sig til et andet ord. Dog vil dt ikke passe med det budskab som designet skal fremvise. Designet er ment til at fremvise, at hvor end man trykker på skærmen, kommer man aldrig videre til næste skridt, idet der befinder sig en fejl i en algoritme bag computeren. 
Min tanke bag det er, at man først lægger mærke til software idet der er en error. For mig er det tit, når jeg bliver frustreret over at min computer ikke virker at jeg kommer i tanke om dets "hårde" arbejde. Jeg havde inspiration fra miniX3, hvor vi programmerede throbbers, da throbbers er en visuel indikator på at computeren er i gang med at arbejde. Denne miniX4 er nærmere efterresultatet af computeren arbejde, der ikke har fungeret.  


![screenshot](https://abishana.gitlab.io/aestetiskprogrammering/miniX4/screenshot_minix4.png)

