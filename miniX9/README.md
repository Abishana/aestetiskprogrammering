# MiniX9
README 

[Link til Alexandra](https://gitlab.com/leealexa12/aestetiskprogrammering/-/tree/main/miniX9) 

[Link til Sultan](https://gitlab.com/SultanDD/sltn/-/blob/main/miniX9/ReadMe.md ) 


[Link til RUNME](https://sultandd.gitlab.io/sltn/miniX9/index.html)

[Link til sketch.js fil](https://gitlab.com/SultanDD/sltn/-/blob/main/miniX9/minix9.js)

[Link til JSON fil](https://gitlab.com/SultanDD/sltn/-/blob/main/miniX9/romtamrus.json)


![screenshot](https://abishana.gitlab.io/aestetiskprogrammering/miniX9/screenshotminix9.png)


#### “How Language Blooms”
For this MiniX we have created a program that highlights the cultural aspects and differences in our group. Since our group consists of members of different cultural backgrounds, such as South-Indian, Eastern-European and Central-Asian, we’ve decided to take advantage of this and create a project which represents our mother-tongues. Our project focuses on the diversity of languages by portraying swear words. As people, we usually connect swear words to a way of releasing frustration while it also being a  way of expressing oneself.  Furthermore one’s vocabulary highlights the way one’s language is influenced by the culture. Swear words are different depending on the language and they have different emotional and cultural impact but they awaken similar emotions such as excitement, frustration and so on .

#### The code
For this MiniX our program runs as a black canvas with random green text popping up in random places on the canvas.  
In the program  we’ve implemented the use of a JSON-file which consists of the text appearing on the screen when running the program. The text written in the JSON-file are swear words and degrading sentences in the languages Romanian, Russian and Tamil. The JSON-file consisting of these words is then loaded into the sketch-file by the use of the syntax `loadJSON`

Hereafter the arrays of words are given their index values according to each language.  A conditional statement here ensures that the outcome  is based on the index value.


     for (let i = 0; i < data.length; i++) {
     let line = data[i].line;
     let id = data[i].id;

     // Angiver index-værdier for vores ARRAYS. Altså, hvis
     udfaldet er under 3, bliver romanian valgt, fra 3-6 tamil
     osv.
     if (id <= 3) {
       romanian.push(line);
     } else if (id <= 6) {
       tamil.push(line);
     } else {
       russian.push(line);
     }
     }


Having done this, the `setup function` is listed with the canvas size and the possible placement of the text appearing on the screen. The `draw function` then has the background value to black, the text color in green, and the framerate of the text is set to one.  Thanks to the draw function, the user gets enough time to  read/have an overview of the text. The `draw function` at last consists of a conditional statement which decides on a random word (array) at a random place on the canvas.


#### Reflection
Language allows people to express themselves and connect to each other by communicating. The text “Vocable code” written by Geoff Cox and Alex McLean argues that programming itself is also a language that gives the programmers the opportunity to express themselves while also connecting to the user. “Computer code has both a legible state and an executable state, namely both readable and writable states at the level of language itself.” (Cox & Soon, 2020). To be able to code and read a code one must learn the different terms and signs, the same way people learn a new language. The way a code is written can also indicate a lot about its programmer, for example some codes are easier to read than others thanks to their structure, same way some books are easier to read than others thanks to the language and techniques used to write the book. A JSON-file makes a JS-file more straightforward and readable by making the code more structured since a big part of the program’s details are in the JSON-file. We can compare a JSON-file to swear words and expressions: one word/phrase, that hides behind a powerful message of either anger, frustration or even excitement, that has a big impact on both the transmitter’s language and the receiver.





