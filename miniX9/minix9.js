let udsagn;
let romanian = [];
let tamil = [];
let russian = [];

function preload() {
  // 
  loadJSON('romtamrus.json', function(data) {
    udsagn = data;
    loadData(udsagn);
  },
  );
}

function loadData(data) {

  // tjekker om der er de korrekte ARRAYS, før vi loader dem

  if (data.romanian && data.tamil && data.russian) {

    // tildeler variabler de tilhørende værdier fra ARRAYS

    romanian = data.romanian;
    tamil = data.tamil;
    russian = data.russian;
  } else {
    
// initialiserer loop 'for' der indhenter linjer/sætninger og tildeler id til ARRAYS

    for (let i = 0; i < data.length; i++) {
      let line = data[i].line;
      let id = data[i].id;

      // Angiver index-værdier for vores ARRAYS. Altså, hvis udfaldet er under 3, bliver romanian valgt, fra 3-6 tamil osv. 
      if (id <= 3) {
        romanian.push(line);
      } else if (id <= 6) {
        tamil.push(line);
      } else {
        russian.push(line);
      }
    }
  }
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  textSize(random(20,35));
  textAlign(CENTER, CENTER);
}

function draw() {
  background(0);
  fill(0,255,0);
  frameRate(1); // sætter lav frameRate, for at gøre koden mindre epileptisk, så den ikke minder om Abishanas koder.

  let randomArray = random([romanian, tamil, russian]);

  if (random(10) < 9) {
    let randomLine = random(randomArray); // der bliver valgt en tilfældig sætning fra et tilfældige ARRAY
    text(randomLine, random(width), random(height)); // udsagn dukker op tilfældige steder på canvasset
  }
}


  