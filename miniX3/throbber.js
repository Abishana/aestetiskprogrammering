function setup() {
    //create a drawing canvas
    createCanvas(windowWidth, windowHeight);
    frameRate(12); 
     
   }
   
   function draw() {
     background(15, 20);  //baggrundsfarve m. alpha værdi (transparens)
     drawElements();
   }
   
   function drawElements() {
     let num = 9; //9 cirkler/mønstre
     
     translate(width/2, height/2); //Koordinatsystemet rykkes til midten af browseren.
     let cir = 360/num*(frameCount%num); //ellipsernes movement/position når de bliver rotated. 
     rotate(radians(cir)); //throbberen roterer

    // Mønstrene (cirkler og ellipser)
     noStroke();
     fill(150, 0, 200);
     ellipse(150, 0, 50, 100);
     fill(100, 0, 200)
     ellipse(150, 0, 100, 50);
     fill(130, 0, 200)
     ellipse(100, 0, 50, 50);
     ellipse(200, 0, 50, 50);
   
    // Discolys
     stroke(random(0,255),random(0,255),random(0,255),20);
     strokeWeight(random(0,255),random(0,255),random(0,255));
     line(60, 0, 60, height);
     line(width-60, 0, width-60, height);
     line(0, 60, width, 60);
     line(0, height-60, width, height-60);

    // if statements
     if (mouseX>windowWidth/2) {
      ellipse(100, 0, 50, 100);
      fill(200, 0, 200)
      ellipse(150, 0, 100, 50);
      fill(230, 0, 200)
      ellipse(100, 0, 50, 50);
      ellipse(200, 0, 50, 50);
    }

      if (mouseY>windowHeight/2) {
        fill(100,60,100)
        ellipse(150, 150, 50, 100);
        circle(0, 0, 100);
      }

   }
   
   function windowResized() {
     resizeCanvas(windowWidth, windowHeight);
   }
   