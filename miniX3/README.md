# MiniX3
README · _Abishana Tharumathas_

[Link til RUNME](https://abishana.gitlab.io/aestetiskprogrammering/miniX3/index.html)
(virker ikke helt så godt på safari)

[Link til js.fil](https://gitlab.com/Abishana/aestetiskprogrammering/-/blob/main/miniX3/throbber.js)

___

##### Hvad er en throbber?

Vi møder ofte throbberen i vores hverdag, men det er ikke tit vi næranalyserer den. Throbberen er for brugeren et tegn på, at noget bag systemet er i gang med at "tænker igennem" noget data. Først når vi møder throbberen, lægger vi mærke til at software ikke fungere optimalt 24/7. Thorbberen er en form for indikator at softwaren glitcher, altså at der bag systemet en form for delay. Når vi som brugerer får vist throbberen på skærmen, er det grundet maskinens arbejde. Trobberen er en visualisering af data, signaler og algoritmer der er i gang med at processere. Maskinetid og mennesketid følger ikke i samme hastighed og vi mennesker har skabt bestemte standardiseringer af tid. Maskinetid er dog modsat mennesketid ikke påvirket af mennesketid. Maskinen følger sin egen tempo med de signaler og data den skal køre igennem. Derfor er en visualisering af dette meget oplagt at få fremvist som bruger, hvilket er den her throbber. 

Når jeg tænker på en throbber er kendetegnet for mig altid den runde cirkel der kører rundt i midten af skærmen. Denne throbber ses f.eks. på Netflix og en Macbook. Netflix har den røde throbber i midten af skærmen, som kører rundt når den er i gang med at loade en film. Throbberen er her en rød linje der cirkulere. På en Macbook ses den samme form for throbber. Her er den cirkulære throbber dog fyldt ud og farvelagt som en regnbue. Her indikerer throbberen også at den er i gang med at "synkronisere" noget data bag skærmen.

Dog støder vi også på andre kreative former for throbbers. Google har et dinosaur spil, imens man venter på at softwaret har gjort sit arbejde. Throbberen er her en form for timekiller, da den er interaktiv på en legende måde. 
Men fælles for alle er, at throbberen er et visuelt billede på et "glitch" i systemet. Og som eksemplerne viser, er der adskillge måder, at visualisere dette på.  

##### Min throbber og forventninger
Jeg har programmeret en throbber med tanker om, at beholde originaliten (cirkulære throbber) af en throbber, men samtidig gør den interessant og interaktionvenlig ligesom Google. 
Min throbber er en throbber i mandalaform, hvor der skydes diskolys ud til alle sider, den er meget farverig. Ved hjælp af `if statements` er browseren interaktionsvenlig. Når musen er ved højre side af browserskærmen, vil mandalen skifte farve fra lilla til lyserød. Når musen horisontalt er ved nedre-delen af skærmen føjes der ekstra former i brun til mandalaen. Når begge krav er fyldt, vil begge handlinger ske samtidig. Men dette var ikke et behov at skrive ind som et `if statement`. 

##### Syntakser
For at beholde programmere min throbber efter min egen perception, har jeg brugt `translate(width/2,height/2)`, for at rykke koordinatsystemet til midten af browseren. Dette gjorde det klart nemmere at programmere resten af designet, da nulpunktet lå i midten af skærmen.

som nævnt vises throbberen i anderledes udseende i forhold til musens placering på browservinduet. Der er programmeret tre versioner af throbberen i forhold til placeringen af musen, hvilket er gjort ved hjælp af ´if-statements´. Under disse ´if-statements´ har jeg så simpelt bare tilføjet flere former (cirkler, ellipser) i andre farver, som tilføjes oveni den originale throbber. 

En lille detalje som jeg spekulerede over i længere tid gennem processen, var den hastighed som throbberen skulle kører rundt i. Da jeg havde disko som tema, vil det være oplagt at få throbberen til at køre hurtigt. Dog skulle thobberen også være visuel nydelig for brugeren, så man også kan opfatte når man interegerer med skærmen.
Derfor satte jeg:`framerate(12)`.


![screenshot](https://abishana.gitlab.io/aestetiskprogrammering/miniX3/screenshot_miniX3.png)

