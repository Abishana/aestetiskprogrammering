
 class Pixel {
    constructor() { 
    this.speed = floor(random(3, 6));
    this.pos = new createVector(width+5, random(12, height/1.7));
    this.size = floor(random(5, 20));
    }
    
  move() {  
    this.pos.x-=this.speed; 
    if (this.pos.x < 0 || this.pos.x > width || this.pos.y < 0 || this.pos.y > height) {
      this.pos.x = random(width);
      this.pos.y = random(height);
    }
  } 
  
  show() { //Layout for pixels
    push()
    translate(this.pos.x, this.pos.y);
    noStroke();
    fill(random(0,255), random(0,255), random(0,255)); 
    rect(0, 0, this.size, this.size);
   pop();
 }
}

