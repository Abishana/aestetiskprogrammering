# MiniX7
README · _Abishana Tharumathas_


[Link til RUNME](https://abishana.gitlab.io/aestetiskprogrammering/miniX7/index.html) **mini epilepsi warning**

[Link til sketch.js fil](https://gitlab.com/Abishana/aestetiskprogrammering/-/blob/main/miniX7/miniX7sketch.js)

[Link til Pixel.js fil](https://gitlab.com/Abishana/aestetiskprogrammering/-/blob/main/miniX7/Pixel.js)


#### Genbesøg af miniX[?]
I denne miniX har vi haft til opgave, at genbesøge en tidligere miniX. Hermed har vi skulle tilføje ændringer og forbedringer til programmet. 
Jeg har valgt at tage udgangspunkt i miniX3, hvor vi programmerede en throbber, med stor inspiration fra miniX6, hvor vi programmerede et spil. Jeg valgte at arbejde med miniX3, da jeg var/er dybt interesseret i en throbbers funktion og visualisering. En throbber trækker på filosofiske tanker/debatter ift. tid. Forskellen på mennesketid og maskinetid, og hvorvidt de begge spiller en rolle i vores hverdag. 

Jeg var helt fascineret af at skabe et spil i miniX6. Det var både spændende i forhold til det tekniske og det kulturelle, men hvis jeg havde brugt længere tid på at sidde med opgaven, vil jeg have programmeret spillet anderledes. Derfor tog jeg stor inspiration fra mit spil og følte at det nu var min chance til at programmere noget jeg vil være 100% tilfreds med.  


#### Mit program
Mit program skal forestille sig at være en throbber. I midten af skærmen er der en diskokugle, som drejer rundt. Denne diskokugle er en gif jeg har fundet på google, der herefter er indskrevet i programmet. Baggrunden består af to dele, den ene er en gif med stjerner, som er indsat i programmet i sammevis som diskokuglen. Den anden del er regnbuefarvet pixels der spawner i baggrunden, hvilket jeg har programmeret ved at bruge `class`. Meget af throbberens kode er taget fra miniX6, hvor jeg programmerede et spil med en diskokugle der fanger pixels. Dog har jeg ændret/slettet på flere syntakser og værdier, så throbberen bl.a. ikke krævede en interaktion. Desuden var mit største ønske at diskokuglen skulle fylde meget i midten af skærmen, og at pixels skulle spawne i baggrunden. En faktor jeg ikke har inkluderet i min kode, som jeg havde overvejet, var baggrundsmusik. 

##### Tidens rolle
En throbber er en viuel skælning mellem maskinetid og mennesketid. Throbberen er med til at viser os at computeren er i gang med at indsamle/gennemarbejde data og algoritmer og dette kan i mennesketid gå hurtigt eller langsomt. Det er derfor også interessant at overveje, hvor hurtigt en computer reagerer i forhold til mennesketid (realtid). Vi mennesker har en opfattelse af hvornår maskinen er for hurtig, langsom eller lige tilpas i forhold til vores perception af tid. Men maskinens tid har vi relativ lidt forstand på, ihvertfald idet den er uafhængig af mennesketid.
Så throbberen viser maskinens bearbejdning i visuel form. Denne bearbejdning indebærer tid og de forskellige opfattelser/perpektiver af tid (maskinetid og mennesketid).


##### Den kulturelle kontekst
I miniX3 havde jeg også programmeret min throbber indenfor diskotema, og dette har jeg igen gjort i miniX7. Dette var ikke intentionelt, men passede meget godt til opgavens kriterier/kontekst. Desuden gav det en interessant vinkel i forhold til den kulturelle konstekst idet de to throbbers program er baseret på farver og diskotema, men har hver deres funktioner og adskiller sig stadig fra hinanden visuelt. 

Disse to throbbers sammenlignet med hinanden er et gost ekspempel på det æstetiske i programmering, altså heriblandt "Creative coding". De to throbbers minder om hinanden i forhold til temaet og deres intention angående throbbers genrelle funktion og betydning for brugeren. Dog er disse throbbers også forskellige i det visuelle og deres funktioner. Throbberen fra miniX3 vil nogle brugere finde spændende idet den er iøjefaldelende med farverne, kan interegeres med og har karakter af en traditionel throbber. Andre vil foretrække miniX7 throbberen, idet den også er iøjefaldende grundet farverne, men den er anderledes idet den har færre karakter af en traditionel throbber. 
Begge throbbers er dermed med til at vise, at en menneskes opfattelse af tid kan på en computer/maskine visualiseres på adskillige måder.


![screenshot](https://abishana.gitlab.io/aestetiskprogrammering/miniX7/screenshot_miniX7.png)
