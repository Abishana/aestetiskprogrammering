let discoballSize = {
    w:300,
    h:330
  };
  
  let discoball;
  let discPosX;
  let discPosY;
  let pixel = [];
  let min_pixel = 50; 
  
  function preload(){
    discoball = loadImage("data/discoball.gif");
    stars = loadImage("data/Stars.gif");
  }
  
  function setup() {
    createCanvas(windowWidth, windowHeight);
    discPosX = width/2;
    discPosY = height/2;
  }

  function draw() {
    background(stars);
    checkPixelNum();
    showPixel();
    image(discoball, discPosX-150, discPosY-250, discoballSize.w, discoballSize.h);
  }
      
  function checkPixelNum() {
    if (pixel.length < min_pixel) {
      pixel.push(new Pixel());
    }
  }

  function showPixel(){
    for (let i = 0; i <pixel.length; i++) {
      pixel[i].move();
      pixel[i].show();
    }
  }
  

  
  