# MiniX6
README · _Abishana Tharumathas_

[Link til RUNME](https://abishana.gitlab.io/aestetiskprogrammering/miniX6/index.html) **mini epilepsi warning**

[Link til sketch.js fil](https://gitlab.com/Abishana/aestetiskprogrammering/-/blob/main/miniX6/miniX6sketch.js)

[Link til Pixel.js fil](https://gitlab.com/Abishana/aestetiskprogrammering/-/blob/main/miniX6/Pixel.js)

##### Hvordan fungerer mit spil?
Jeg har programmeret et spil der ligner ToFuGo-spillet. Inden vi decodede dette spil, havde jeg allerede i tankerne at programmere et spil ligesom dette. Derfor er det meste af min kode også inspireret af dette spil. Mit spil går ud på at få en discokugle til at spise pixels. Dette fungere ved at benytte UP & DOWN ARROWS på tastaturet. Spillets regler indebærer, at discokuglen skal spise flere pixels end den misser. Hvis den har misset flere pixels end den har spist, vil spillet stoppe og der vil stå "GAME OVER" på skærem.

###### Koden for spillet
I min kode har jeg inden `setup()` funktionen defineret forskellige variabler for både diskokuglen og pixels. Desuden har jeg første gang brugt `preload()` funktionen til at indsætte en gif af en discokugle. Denne gif fandt jeg på nettet, hvilket jeg herefter har defineret i `preload()` funktionen som: `discoball = loadImage("data/discoball.gif")`. giffen er defineret som discoball, så dette kan benyttes gennem resten af koden.

I min `draw()` funktion har indskrevet flere funktioner, der får tildelt deres roller/værdier senere i koden. Funktionen `displayScore()` sørger for at vise de antal pixels diskokuglen indsamler og mister. `showPixel()` funktionen sørger for at pixels bevæger fra højre til venstre på skærmen, så diskokuglen kan fange disse. I denne funktion er der et for-loop, der har som condition at udføre: 

`pixel[i].move(); pixel[i].show();`

move og show ser vi ikke defineret i denne js.fil. Dette kommer fra en anden js.fil, hvor jeg har "classified" Pixel. Ved at sætte `class Pixel` kan classet både tilskrives funktioner og conditions. Her har jeg givet `Pixel` dens parametre ved bl.a. at tilskrive den en ``move()` og ``show()` funktion, der netop indebærer dens bevægelse og udseende.

For at kunne spille dette spil, skal der være en form for interaktion. Denne interaktion er kodet vha. `keyPressed()` funktionen, der sørger for at diskokuglen flytter op eller ned, idet man trykker på UP & DOWN ARROW tasterne. Dette er meget essentiel, da spillet er baseret på at spilleren selv skal trykke på disse taster, for at få diskokuglen til at spise dens lys/pixels.


##### Objekt-orienteret programmering 
Fokuspunktet i denne miniX har primært været obejkt-orienteret programmering. Objekt-orienteret programmering er prorgammering der har fokus på data og objekter fremfor funktioner og logikker. Dette medvirker også, at `class` får en vigig rolle, idet denne fil beskriver objektets egenskaber. I dette spil beskriver ``class` de pixels der bevæger sig fra højre til ventre på skærmen samtidig med at de lyser i forskellige farver. 

Dette spil, som man nok kan regne ud, forestiller sig at være en diskokugle, der prøver at opfange sine lys. Mit spil er særdeles abstrakt idet den repsræsenterer noget festligt og vildt på en forsimplet måde. Desuden absorberer diskokugler ikke lys, men reflekterer. 

###### Hvorfor går mit spil ud på at fange lys/pixels? 
Dette har jeg gjort af flere årsager. Både fordi det var en sjov detalje, at noget er "mindre realistisk", hvilket i samme dur med til at vise, at der er flere perspektiver af samme sag. For nogle kan det virke at en diskolys et et spejl der reflekterer lys, for andre at den bare lyser eller at den som i mit spil absorberer lys. Nogle af disse perspektiver er mere rigtige end andre, især at en diskolys absorberer lys, har jeg ikke hørt om før. Men i forhold til spillet er dette faktisk det mest passende perspektiv. Det er med til at vise, at spil kan være med til at fremvise en anden realitet, med inspiration fra den realitet vi lever i. Selvom jeg i det visuelle og interaktionen ikke har tydeliggjort det i spillet, havde jeg stadig i bagtankerne at vil inddrage dette.

![screenshot](https://abishana.gitlab.io/aestetiskprogrammering/miniX6/screenshot_miniX6.png)
