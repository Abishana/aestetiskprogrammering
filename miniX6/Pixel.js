
 class Pixel {
    constructor() { 
    this.speed = floor(random(3, 6));
    this.pos = new createVector(width+5, random(12, height/1.7));
    this.size = floor(random(15, 35));
    this.emoji_size = this.size/1.8;
    }
  move() {  
    this.pos.x-=this.speed;  
  }
  show() { //Layout for pixels
    push()
    translate(this.pos.x, this.pos.y);
    noStroke();
    fill(random(0,255), random(0,255), random(0,255)); 
    rect(0, 0, this.size, this.size);
   pop();
 }
}

