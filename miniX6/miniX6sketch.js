let discoballSize = {
    w:86,
    h:94
  };
  
  let discoball;
  let discPosY;
  let mini_height;
  let min_pixel = 5; 
  let pixel = [];
  let score =0, lose = 0;
  let keyColor = 45;
  
  function preload(){
    discoball = loadImage("data/discoball.gif");
  }
  
  function setup() {
    createCanvas(windowWidth, windowHeight);
    discPosY = height/2;
    mini_height = height/2;
  }

  function draw() {
    background(30);
    fill(keyColor, 255);
    rect(0, height/1.5, width, 1);
    displayScore();
    checkPixelNum(); 
    showPixel();
    image(discoball, 0, discPosY, discoballSize.w, discoballSize.h);
    checkEating(); 
    checkResult();
  }
  
  function checkPixelNum() {
    if (pixel.length < min_pixel) {
      pixel.push(new Pixel());
    }
  }
  
  function showPixel(){
    for (let i = 0; i <pixel.length; i++) {
      pixel[i].move();
      pixel[i].show();
    }
  }
  
  function checkEating() { //Conditions for om discoball spiser pixels
    for (let i = 0; i < pixel.length; i++) {
      let d = int(
        dist(discoballSize.w/2, discPosY+discoballSize.h/2,
          pixel[i].pos.x, pixel[i].pos.y)
        );
      if (d < discoballSize.w/2.5) { 
        score++;
        pixel.splice(i,1);
      }else if (pixel[i].pos.x < 3) { 
        lose++;
        pixel.splice(i,1);
      }
    }
  }
  
  function displayScore() { //scoring display
      fill(230);
      textSize(17);
      textStyle(BOLD);
      text('You have collected '+ score + " pixels", 10, height/1.4);
      text('You have lost ' + lose + " pixels", 10, height/1.4+20);
      text('PRESS the ARROW UP & DOWN key to collect the pixels',
      width/4, height/1.4+40);
  }
  
  function checkResult() { //Game over display
    if (lose > score && lose > 3) {
      fill(255,0,0);
      textSize(40);
      text("GAME OVER", width/3, height/1.4);
      noLoop();
    }
  }
  
  function keyPressed() {
    if (keyCode === UP_ARROW) {
      discPosY-=25;
    } else if (keyCode === DOWN_ARROW) {
      discPosY+=25;
    }
    //discoball bliver indenfor skærmen
    if (discPosY > mini_height) {
      discPosY = mini_height;
    } else if (discPosY < 0 - discoballSize.w/2) {
      discPosY = 0;
    }
  
    return false;
  }
  
  