let circles = [];
let numCircles = 10;

function setup() {
  createCanvas(700, 700);
  noStroke();
   /* Forloopet indebærer at numCircles får 
  tildelt flere outcomes af cirkler.
  For hvert nyt cirkel, kommer der en random
  værdi af størrelse, position og hastighed på.*/
  /* new Circle() har push funktion idet denne 
  syntaks tildeler circle() en nye "værdier
  til circles arrayet*/
  for (let i = 0; i < numCircles; i++) {
    circles.push(new Circle(random(width), random(height), random(10, 30)));}
}

function draw() {
  background(0);
  /*Forloopet sørger for, at der er et 
  bestemt movement og layout for cirklen
  for alle værdier i circles arrayet. Derfor
  står der også circles.length-1, idet 
  værdien 0 hermed også kommer med i 
  statementet.*/
  for (let i = circles.length - 1; i >= 0; i--) {
    circles[i].move();
    circles[i].display();
     /*Her er en nested-loop, der sørger for at 
    ved stødning af to cirkler, bliver den 
    lille cirkel spist af den store cirkel. 
    Splice() sørger for at fjerne den lille
    cirkel ved stødningen*/ 
    for (let j = circles.length - 1; j >= 0; j--) {
      if (i != j && circles[i].intersects(circles[j])) {
        circles[i].eat(circles[j]);
        circles.splice(j, 1);
      }
    }
  }
}

class Circle {
  constructor(x, y, r) {
    this.pos = createVector(x, y); //position
    this.vel = createVector(random(-3, 3), random(-3, 3)); //hastighed
    this.r = r; //radius
    this.color = color(random(255), random(255), random(255)); //farve
  }
  
  move() {
    this.pos.add(this.vel); //hastighedsvektor sættes ind i positionsvektoren.
    //if-statementet sørger for, at cirklerne ikke hopper ud over canvasset.
    if (this.pos.x < 0 || this.pos.x > width) {
      this.vel.x *= -1;
    }
    if (this.pos.y < 0 || this.pos.y > height) {
      this.vel.y *= -1;
    }
  }
  
  display() {
    fill(this.color);
    ellipse(this.pos.x, this.pos.y, this.r * 2);
  }
  
  intersects(other) {
    let d = dist(this.pos.x, this.pos.y, other.pos.x, other.pos.y);
    return d < (this.r + other.r) / 2; 
    //Checker om cirklerne støder ind i hinanden med en radius/2.
  }
  
  eat(other) {
    this.r += other.r / 2;
    this.vel.add(other.vel); 
    this.color = lerpColor(this.color, other.color, 0.5);
    /*cirklerne spiser hinanden. Ved sidste linje dannes en farve for cirklen
    der ligger gennemsnitligt mellem de to farver ved hjælp af lerpColor funktionen.*/
  }
}
