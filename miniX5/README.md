# MiniX5
README · _Abishana Tharumathas_

[Link til RUNME](https://abishana.gitlab.io/aestetiskprogrammering/miniX5/index.html)
(Der er en tendens til at programmet lacker. Hvis dette sker, så åben programmet igen)

[Link til js.fil](https://gitlab.com/Abishana/aestetiskprogrammering/-/blob/main/miniX5/miniX5sketch.js)



#### Mit generative program
##### Tanker gennem programmeringsprocessen
Da jeg i forvejen har haft svært ved at forstå meningen med loops, var dette ikke en nem opgave. Derfor brugte jeg meget tid på at tænke over de elementer jeg vil inddrage i programmet, inden jeg begyndte med at programmere. Jeg havde en ide om, at mit program skulle være cirkler der hoppede rundt i canvasset. Herudover vil jeg implementere en "konkurrence" mellem cirklerne. Dette fik jeg med hovedkløen og rigeligt hjælp succesfuldt programmeret.

##### Hvad har jeg programmeret?
Mit program er et sort canvas, hvor der er cirkler, som flyver/hopper rundt på skærmen. Når de rammer væggene af canvasset hopper de tilbage i en ny retning. Cirklerne har forskellige farver, størrelser og hastigheder, hvilket jeg har brugt `random()` funktionen til. 
Derudover for at skabe en form for konkurrence mellem cirklerne, har jeg programmeret cirklerne til at spise hinanden, når de støder sammen. Derfor vil man realtiv hurtigt opleve, at der kun er en cirkel tilbage på skærmen. 

###### Koden
Selve koden var en lang process for mig at skrive og forstå, men efter en masse hjælp, tænkning og "prøve sig frem", har jeg en relativ god forståelse for min kode. Derfor har jeg også i denne miniX program føjes lange kommentarer inde i koden, for at huske de mindre detaljer.

Min kode er generativt idet jeg bruger for-loops, while-loop og `random()` syntaksen. Random syntaksen er benyttet flere steder i koden bl.a. for at bestemme størrelsen og hastigheden på de cirkler der dannes ved start. For at danne disse cirkler har jeg først skrevet to variabler, et array og arrayets længde. Herefter har jeg i min `setup()` funktion skrevet et for-loop der danner nye cirkler ud fra tilfældighed:
`for (let i = 0; i < numCircles; i++) {circles.push(Circle(random(width),random(height),random(10, 30)));` 
   
I min ´draw()´ funktion har jgg igen skrevet et for-loop der sørger for alle cirkler i arrayet får et bestemt layout og bevægelse.
Desuden har jeg herunder lavet et conditional statement i en nested-loop idet, at hvis cirklerne støder ind i hinanden, spises de og bliver til en større cirkel.
 `for (let j = circles.length - 1; j >= 0; j--) 
 {if (i != j && circles[i].intersects(circles[j])) 
 {circles[i].eat(circles[j]);
circles.splice(j, 1); }}`

I mine loops bruger jeg `move(), display(), intersects(other) og eat(other)` i syntakerne. Disse bliver dog tildelt en kode senere i programmet, der giver cirklerne deres funktion.

#### Ideen om "auto-generator"
Mit program kunne være tilpasses bedre indenfor det autogenerende, hvis cirklerne genererede efter at spise hinanden. Dog gik jeg med at cirklerne ikke generede igen efter at have spist hindanden idet jeg havde "konkurrence" som en af mine startkriterier for programmet.
Jeg havde et ønske om, at cirklerne skulle generere igen efter alle var blevet spist, og der kun var en tilbage på canvasset. Dog var programmeringsprocessen en stor udfordring både skill- og tidsmæssigt i forvejen og derfor valgte jeg ikke at inkludere dette.

Mit program er dog generativt idet cirklerne dannes og er i bevægelse uden direkte interaktion. Ved hjælp af loops og conditional statements er der en hierarki mellem cirklerne og programmet er bestemt til at cirklerne udføre en en bestemt tilfældig handling idet jeg har brugt `random()` funktionen.

![screenshot](https://abishana.gitlab.io/aestetiskprogrammering/miniX5/screenshot_miniX5.png)
