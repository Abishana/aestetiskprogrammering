# MiniX10

[Link to program](https://editor.p5js.org/ml5/sketches/Sentiment_Interactive)


[Link to Alexandra](https://gitlab.com/leealexa12/aestetiskprogrammering/-/tree/main/miniX10) 

[Link to Sultan](https://gitlab.com/SultanDD/sltn/-/blob/main/miniX10/ReadMe.md ) 

##### Sentiment
We’ve chosen the code “Sentiment”. We think this code is interesting since it rates the sentiment of a written text. The sentiment is based on the meaning of the text which for us seems subjective. We’ve tried several sentences which scored:

“My wife died” - 0,92

“My dog died” - 0,01

“I am danish” - 0,71

I am romanian” - 0,00

 The rating scale stretches from 0 to 1 in which zero is negative and one is positive. It rates based on words from movie reviews in which only 20.000 of the most common words are used from here. Therefore as the examples show, the results of sentiment are inaccurate. 

##### Changed syntaxes & parameters
The  rating scale is classified in the index file and is referred to in the sketch file as ´sentimentResult.html´. 
We have, among other things, changed the 'score' value, as we thought that 0-1 (including decimals) was too narrow. Values like 0, 50 and 100 are more well-known in broader circles, since we  people are more used to expressions like ‘50/50’ and ‘100%’. 
That is why we have changed the following line:


![screenshot](https://abishana.gitlab.io/aestetiskprogrammering/miniX10/sentimentresult.png)


where we have used the built-in function ´math.round´, that rounds the given values to the nearest integer and then multiplied it by 100, as seen below; 

![screenshot](https://abishana.gitlab.io/aestetiskprogrammering/miniX10/sentimentresultchange.png)


In the code we found a few syntax which  we didn’t have knowledge of before. A few of these are ´createP´, ´modelReady´, and so on. 
´createP´ is a syntax which shows the given value of sentiment of the written text on the canvas. 
´ModelReady´ is a callback function that is called when a machine learning model has finished loading and is ready to make predictions.


##### Restrictions in the code
The program is limited to the initial parameters used to train the default model. The user is allowed to write a maximum of 200 words and based on the 20.000 most used words in the reviews from IMDB, the user gets a rating from 0 to 1, zero being negative and 1 being positive. The program is therefore dependent on the machine learning algorithms. We have explored the functions and capacity of the program by writing different sentences with different emotional impacts such as the death of a pet and the sunny weather. Furthermore we have tested the program by using neutral sentences involving nationalities. 

The program can be compared to the very modern form of AI called ChatGPT. Sentiment is a machine unlearning that works using an already existing database collected from IMDB, being restricted to its parameters. In contrast to this program is  ChatGPT that works based on an active machine learning which helps the algorithm to improve and evolve, meaning that the AI is not as restricted as the program Sentiment. 


##### Questions/things we’re curious about
Why did they code  the rating scale from 0 to 1? - 0-100 made more sense for us.
What do you think could be the potential implications and restrictions of such ‘AI’?

