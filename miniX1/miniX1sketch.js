function setup() {
  // put setup code here
 
  createCanvas(500,500);
  background(200,150,200);

  fill(200,140,200);
square(0,0,20) //1. linje
square(40,0,20)
square(120,0,20)
square(80,0,20)
square(160,0,20)
square(200,0,20)
square(240,0,20)
square(280,0,20)
square(320,0,20)
square(360,0,20)
square(400,0,20)
square(440,0,20)
square(480,0,20)
square(20,20,20) // 2. linje
square(60,20,20)
square(100,20,20)
square(140,20,20)
square(180,20,20)
square(220,20,20)
square(260,20,20)
square(300,20,20)
square(340,20,20)
square(380,20,20)
square(420,20,20)
square(460,20,20)
square(0,40,20) // 3. linje
square(40,40,20)
square(80,40,20)
square(120,40,20)
square(160,40,20)
square(200,40,20)
square(240,40,20)
square(280,40,20)
square(320,40,20)
square(360,40,20)
square(400,40,20)
square(440,40,20)
square(480,40,20)

  fill(200,150,200);
square(20,0,20) //1. linje
square(60,0,20)
square(100,0,20)
square(140,0,20)
square(180,0,20)
square(220,0,20)
square(260,0,20)
square(300,0,20)
square(340,0,20)
square(380,0,20)
square(420,0,20)
square(460,0,20)
square(0,20,20) // 2. linje
square(40,20,20)
square(80,20,20)
square(120,20,20)
square(160,20,20)
square(200,20,20)
square(240,20,20)
square(280,20,20)
square(320,20,20)
square(360,20,20)
square(400,20,20)
square(440,20,20)
square(480,20,20)
square(20,40,20) // 3. linje
square(60,40,20)
square(100,40,20)
square(140,40,20)
square(180,40,20)
square(220,40,20)
square(260,40,20)
square(300,40,20)
square(340,40,20)
square(380,40,20)
square(420,40,20)
square(460,40,20)


}

function draw() {
  // put drawing code here

//midtercirkel
  fill(200,200,100);
  circle(250,250,100)

  //millitærgrøn midtercirkel
fill(150,150,100)
circle(250,250,30)
  
//yderste brune cirkler (store)
fill(170,140,100)
circle(150,150,150)
circle(350,150,150)
circle(350,350,150)
circle(150,350,150)

//lysebrune cirkler i yderste brune cirkler
fill(180,140,100)
circle(150,150,100)
circle(350,150,100)
circle(350,350,100)
circle(150,350,100)

//ydre grønne cirkler (normal)
  fill(180,200,100);
  circle(250,350,100)
  circle(350,250,100)
  circle(250,150,100)
  circle(150,250,100)

//grønne midtercirkler (normal)
  fill(160,160,100);
  circle(200,200,100)
  circle(300,300,100)
  circle(300,200,100)
  circle(200,300,100)

//brune cirkler i grønne cirkler
fill(170,150,100);
circle(200,200,90)
circle(200,300,90)
circle(300,200,90)
circle(300,300,90)

//grønne cirkler i indre brune cirkler (mini)
fill(160,160,100);
circle(200,200,40)
circle(200,300,40)
circle(300,200,40)
circle(300,300,40)

//ydre brune cirkler (små)
fill(180,150,130)
circle(250,430,30)
circle(430,250,30)
circle(70,250,30)

//lyserøde cirkler i ydre brune cirkler (mini)
fill(180,140,130)
circle(250,430,10)
circle(430,250,10)
circle(70,250,10)
circle(250,470,10)
circle(470,250,10)
circle(30,250,10) 
circle(250,86,10) //tre øverste små cirkler
circle(245,95,10)
circle(255,95,10)

}
