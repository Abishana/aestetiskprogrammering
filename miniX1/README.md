# README · MINIX1

[link til RUNME](https://Abishana.gitlab.io/aestetiskprogrammering/miniX1/index.html)

[Link til js.fil](https://gitlab.com/Abishana/aestetiskprogrammering/-/blob/main/miniX1/miniX1sketch.js)


## Mit færdige produkt
Jeg har ved hjælp af p5.js, som tager udgangspunkt i programmeringssproget Javascript, lavet en programmering inde på Visual Studio Code. Da dette er min første programmering, var min intention, at eksperimentere med er par koder, samtidig med at bevare æstetikken (i form af skønhed), simpliciteten og personligheden i designet. Dette fik jeg udført relativ fint, idet jeg i programmeringen visualiserer et canvas med en mandala. Mandalaen har betydning for mig idet jeg laver hennatatoveringer, hvor blomster og detaljerede mandalaer som regel er i stor fokus. Brun, grøn og lilla er mine yndlingsfarver, hvilket medfører at jeg bevidst inkluderede disse farver som centrale farver i designet. 



### Første indtryk
Dette var min første programmering. Jeg synes det er udfordrende, men samtidig fascinerende at man ved et par ord og parametre kan få et visuelt design op at køre. 
Det er meget interessant, at der findes mange forskellige programmeringssprog, der hver især har sin egen syntaks. Når vi koder, skriver vi i menneskesprog (source code), som computeren oversætter til object code. Selvom mit design ikke indeholder indviklede koder, har det stadig været en faktor, der har kørt i baghovedet idet de mindste detaljer stadig har haft stor betydning. 


## Bedre designer
De små firkanter øverst på canvasset var mit forsøg på at lave en avanceret-lignende baggrund. Dog var jeg ikke ambitiøs nok til at færdiggøre det, som jeg selv havde visualiseret det, da jeg ikke kendte en bedre måde at lave det på, uden at have alt for mange _square()_ koder i min programmering. Jeg håber ihvertfald at kunne finde en bedre løsning til dette næste gang.
I programmeringsprocessen tænkte jeg på et tidspunkt, at det kunne være fedt at programmere en baggrund der skiftede farve. Dette var en udfordring, som jeg derfor endte med at søge på nettet efter. Her fandt jeg koder, som andre havde nedskrevet, hvilket jeg kopierede ind i min programmering. Selvom disse koder ikke fungerede gik forsøgene ikke til spilde. For det første, er der meget vigtigt at forstå de koder man skriver ned og kopierer fra andre kilder. Det er essentielt at kende til alle ledende i ens programmering, da de mindste detaljer gør sig gældende. Derfor kopierede jeg ikke koder fra andre kilder, som jeg ikke havde en forståelse for.
Det er også meget vigtigt at jeg forstår de koder som min programmering indeholder, idet programmering både er et redskab og sprog til at udtrykke mine værdier og kompetencer som designer. Derfor synes jeg også at miniX er med til at styrke min viden indenfor mine egne værdier og kompetencer. Dette medvirker at jeg kan blive en bedre programmør og designer.

![screenshot](https://abishana.gitlab.io/aestetiskprogrammering/miniX1/miniX1_-_visual_sketch.png)
